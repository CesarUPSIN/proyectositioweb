// Import the functions you need from the SDKs you need
import { initializeApp }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import{ getDatabase,onValue,ref,set,child,get,update,remove }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import { getStorage,ref as refS, uploadBytes, getDownloadURL } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
apiKey: "AIzaSyB_GTKpxhk8lJIxZVhIxJejTxSMTdR0NyA",
authDomain: "final-9e274.firebaseapp.com",
databaseURL: "https://final-9e274-default-rtdb.firebaseio.com",
projectId: "final-9e274",
storageBucket: "final-9e274.appspot.com",
messagingSenderId: "571952330113",
appId: "1:571952330113:web:1b191d75fd799a8c916480"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

var btnMostrar= document.getElementById('btnMostrar');
var lista = document.getElementById('lista-productos');

// Variables inputs
var codigo = 0;
var url2="";
var archivo="";
var obtenerURL=" ";

window.onload = function mostrarProductos() {
    const db = getDatabase();
    const dbref = ref(db, 'productos');

    onValue(dbref, (snapshot)=>{

        snapshot.forEach((childSnapshot)=> {
            const chilData = childSnapshot.val();
            archivo = chilData.archivo;

            cargarProductos(chilData.nombre, chilData.precio, chilData.descripcion, chilData.estado);
        });
    }, {
        onlyOnce: true
    });
}

function cargarProductos(nombre, precio, descripcion, cEstado){
    const storage = getStorage();
    const starsRef = refS(storage, 'imagenes/'+ archivo);
    lista.innerHTML = "";

    // Get the download URL
    getDownloadURL(starsRef)
    .then((url) => {
        if(cEstado == 1) {
            lista.innerHTML = lista.innerHTML + "<div class='producto'>"+"<picture><img src="+url+">"+"</picture><h3>"+nombre+"</h3><span>"+"$"+precio+"</span><p>"+descripcion+"</p><button>"+"Saber más"+"</button></div>";
        }
    })
    .catch((error) => {
        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
        case 'storage/object-not-found':
            alert("No existe el archivo");
            break;
        case 'storage/unauthorized':
            alert("No tiene permisos");
            break;
        case 'storage/canceled':
            alert("Se canceló la subida")
            break;
        // ...
        case 'storage/unknown':
            alert("Error desconocido");
            break;
        }
    });
}
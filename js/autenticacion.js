// Import the functions you need from the SDKs you need
import { initializeApp }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import{ getDatabase,onValue,ref,set,child,get,update,remove }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import { getStorage,ref as refS, uploadBytes, getDownloadURL } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

import { getAuth, signInWithEmailAndPassword }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
apiKey: "AIzaSyB_GTKpxhk8lJIxZVhIxJejTxSMTdR0NyA",
authDomain: "final-9e274.firebaseapp.com",
databaseURL: "https://final-9e274-default-rtdb.firebaseio.com",
projectId: "final-9e274",
storageBucket: "final-9e274.appspot.com",
messagingSenderId: "571952330113",
appId: "1:571952330113:web:1b191d75fd799a8c916480"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();


var btnIngresar = document.getElementById('btnIngresar');

function login() {
    let correo = document.getElementById('txtCorreo').value;
    let pass = document.getElementById('txtPass').value;

    const auth = getAuth();
    signInWithEmailAndPassword(auth, correo, pass)
    .then((user) => {
        alert('Bienvenido!');
        window.location.href = "/html/panelAdmin.html";
    })
    .catch((error) => {
        alert("NEL");
    });
}

btnIngresar.addEventListener('click', login);